def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))

class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s): #metodo especial --> si empieza por __ --> def __init__
        self.nombre = n
        self.nomina = s

    def CalculoImpuestos (self): #funcion dentro de una clase --> metodo
        impuestos = self.nomina*0.30
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
        tax=impuestos))
        return impuestos

empleadoPepe = Empleado("Pepe", 20000) #variables = instancias
empleadaAna = Empleado("Ana",30000)
displayCost(empleadoPepe.CalculoImpuestos() +  empleadaAna.CalculoImpuestos())
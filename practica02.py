def CalculoImpuestos (nombre, nom):
    impuestos = nom*0.30
    print ("El empleado {name} debe pagar {tax:.2f}".format(name=nombre,
    tax=impuestos))
    #El format sirve para imprimir de manera más elegante, con {} se crean huecos en la frase (string)
    #para meter información que aparece después del format, el :.2f espara poner solo dos decimales
    return impuestos

def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))

class Empleado:
    """Un ejemplo de clase para Empleados"""
    #clase es como una plantilla --> construye conceptos
    #cualquier objeto de tipo empleado que cree contendrá un nombre y una nomina (parámetros)
    def __init__(self, n, s): #constructor --> le dice a python como se construyen los empleados 
        self.nombre = n       #tiene como objetivo --> crear objetos de la clase Empleado
        self.nomina = s       #1er param --> siempre self pondre tantos argumentos (self.prop = p) como tenga a continuac del self

empleadoPepe = Empleado("Pepe", 20000)  #empleadoPepe es un objeto de tipo Empleado con dos prop (nombre y nomina)
empleadaAna = Empleado("Ana",30000)
displayCost(CalculoImpuestos(empleadoPepe.nombre, empleadoPepe.nomina) +
 CalculoImpuestos(empleadaAna.nombre, empleadaAna.nomina))